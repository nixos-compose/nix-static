# NIX static helper

The <code>bin</code> directory contains some static version of NIX for standalone and frictionless installation (e.g. HPC cluster). These versions are built from official code look at  <code>build-nix-static.sh</code> for the build command. For manual installation just copy the version chosen executable and rename it to nix. An convenient installer script is also provided, nix executable will be install in <code>$HOME/.local/bin</code>. To run installer script: 
```console
     curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/install-nix-static.sh | bash -
``` 

Experimetal version to support multi-architecture (<code>x86_64</code>, <code>aarch64/</code>):
```console
     curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/install-multi-arch-nix-static.sh | bash -
``` 

**Note:** At each invocation of these scripts <code>nix</code> will be overriden if it's present. 
