#alias s := sg

default:
    @just --list

get-lastest-nix:
    ./build-nix-static.sh
    
g5k-install SITE:
    ssh {{SITE}}.g5k "curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/install-nix-static.sh | sh -"

g5k-install-multi SITE:
    ssh {{SITE}}.g5k "curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/install-multi-arch-nix-static.sh | sh -"

set-default NIX_VERSION:
     #!/usr/bin/env bash
     for arch in "x86_64" "aarch64" #"riscv64" "powerpc64le"
     do
         nix_version_arch_linux_static=nix-{{ NIX_VERSION }}-$arch-linux-static
         if [ -f "bin/$nix_version_arch_linux_static" ]; then
             rm -f bin/nix-$arch-linux-static
             ln -s $nix_version_arch_linux_static bin/nix-$arch-linux-static
         else
             echo "WARNNING:  $nix_version_arch_linux_static doesn't exist"
         fi
     done  
