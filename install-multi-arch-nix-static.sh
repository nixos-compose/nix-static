#!/bin/bash

LATEST_NIX=$(curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/bin/nix-x86_64-linux-static)

if [ -z "$LATEST_NIX" ]
then   
   echo "Failed to retrieve LATEST_NIX"
   exit 1
fi

echo "Latest nix: $LATEST_NIX"

mkdir -p $HOME/.local/bin

if [ -f "$HOME/.local/bin/nix" ] ; then
    rm "$HOME/.local/bin/nix"
fi

for arch in "x86_64" "aarch64"
do
    mkdir -p $HOME/.local/bin/$arch  
    LATEST_NIX=$(curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/bin/nix-$arch-linux-static)         
    curl -sSf "https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/bin/$LATEST_NIX" -o $HOME/.local/bin/$arch/nix 
    chmod 755 $HOME/.local/bin/$arch/nix
done

LOCAL_BIN=$HOME/.local/bin

if [ -z "$LOCAL_BIN/nix" ]
then
    echo "Create a nix proxy script for multi-archi support"
else    
    echo "Override nix command"
fi    

cd $LOCAL_BIN
cat <<EOF > nix-multi-arch.sh
#!/usr/bin/env bash
NIX_COMMAND=$HOME/.local/bin/\$(uname -m)/nix
exec \$NIX_COMMAND \$@
EOF
chmod 755 nix-multi-arch.sh
ln -s nix-multi-arch.sh nix
cd - > /dev/null

if ! [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
  echo "Your path is missing ~/.local/bin, you might want to add it to access to nix command."
fi
