#!/usr/bin/env bash

ASKED_NIX_TAG=$1

GH_TOKEN=$(cat ~/tokens/github_token_ro)

if [ -z "$GH_TOKEN" ]
then   
   echo "Failed to retrieve GH_TOKEN"
   exit 1
fi

if [ -z "$ASKED_NIX_TAG" ]
then
   NIX_TAG=$(curl -sSf -L \
     -H "Accept: application/vnd.github+json" \
     -H "Authorization: Bearer $GH_TOKEN" \
     -H "X-GitHub-Api-Version: 2022-11-28" \
     https://api.github.com/repos/NixOS/nix/tags | jq '.[0]')
else
   NIX_TAG=$(curl -sSf -L \
     -H "Accept: application/vnd.github+json" \
     -H "Authorization: Bearer $GH_TOKEN" \
     -H "X-GitHub-Api-Version: 2022-11-28" \
     https://api.github.com/repos/NixOS/nix/tags | \
     jq --arg ASKED_NIX_TAG "$ASKED_NIX_TAG" '.[ ] | select(.name==$ASKED_NIX_TAG)')
fi

if [ -z "$NIX_TAG" ]
then   
    echo "Failed to retrieve NIX_TAG with select tag: $SELECT_NIX_TAG"
    exit 1
fi

NIX_VERSION=$(echo $NIX_TAG | jq -r .name)
NIX_SHA=$(echo $NIX_TAG | jq -r .commit.sha)

if [ -z "$ASKED_NIX_TAG" ]
then    
    echo "Latest NIX version: $NIX_VERSION"
    echo "Latest NIX sha: $NIX_SHA"
fi

for arch in "x86_64" "aarch64" #"riscv64" "powerpc64le"
do
    echo  nix build "github:NixOS/nix/$NIX_SHA#packages.$arch-linux.nix-static"
    rm result
    nix build "github:NixOS/nix/$NIX_SHA#packages.$arch-linux.nix-static" || continue
    cp -f result/bin/nix bin/nix-$NIX_VERSION-$arch-linux-static
    #if [ -z "$ASKED_NIX_TAG" ]
    #then
    #    rm -f bin/nix-$arch-linux-static
    #    ln -s nix-$NIX_VERSION-$arch-linux-static bin/nix-$arch-linux-static
    #fi
done
