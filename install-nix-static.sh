#!/bin/bash

LATEST_NIX=$(curl -sSf -L https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/bin/nix-x86_64-linux-static)

if [ -z "$LATEST_NIX" ]
then   
   echo "Failed to retrieve LATEST_NIX"
   exit 1
fi

echo "Latest nix: $LATEST_NIX"

mkdir -p $HOME/.local/bin

if [ -f "$HOME/.local/bin/nix" ] ; then
    rm "$HOME/.local/bin/nix"
fi

curl -sSf "https://gitlab.inria.fr/nixos-compose/nix-static/-/raw/main/bin/$LATEST_NIX" -o $HOME/.local/bin/nix 
chmod 755 $HOME/.local/bin/nix

if ! [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
  echo "Your path is missing ~/.local/bin, you might want to add it to access to nix command."
fi
